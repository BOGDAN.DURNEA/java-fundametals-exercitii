package com.sda.company.version2;

import java.time.LocalDate;
import java.time.Period;

public class Employee {
    private String name;
    private LocalDate birthDate;


    public Employee() {
    }

    public Employee(String name, LocalDate birthDate) {
        this.name = name;
        this.birthDate = birthDate;
    }

    public int findAge() {
        LocalDate today = LocalDate.now();
        int age = (Period.between(this.birthDate, today)).getYears();
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

}
