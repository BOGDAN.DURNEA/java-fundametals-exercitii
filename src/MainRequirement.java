package com.sda.company.version2;

import java.time.LocalDate;

/**
 * A company is composed of many employees. Each company has a name as identifier.
 * Each employee should have a name and a date of birth.
 * Compute the average age of the employees of a company.
 */
public class MainRequirement {

    public static void main(String[] args) {

        Employee emp1 = new Employee("Ionescu G.", LocalDate.of(1991, 11, 24));

        Employee angajat1 = new Employee("Descultescu A.",LocalDate.of(1976,2,13));
        Employee angajat2 = new Employee("Muraru A.",LocalDate.of(1999,3,10));
        Employee angajat3 = new Employee("Moise A.",LocalDate.of(1990,4,15));
        Employee angajat4 = new Employee("Petrariu C.",LocalDate.of(1985,8,22));


        Employee emp2 = new Employee("Petrescu J.", LocalDate.of(1992, 10, 23));
        Employee emp3 = new Employee("Petrdcu J.", LocalDate.of(1984, 9, 28));
        Employee emp4 = new Employee("Pebgbhfescu J.", LocalDate.of(1978, 8, 6));
        Employee emp5 = new Employee("Petrdssfscu J.", LocalDate.of(1945, 6, 15));
        Employee emp6 = new Employee("Pesfescu J.", LocalDate.of(1956, 8, 7));
        Employee emp7 = new Employee("Petffgfscu J.", LocalDate.of(1982, 2, 2));
        Employee emp8 = new Employee("Comanescu B.", LocalDate.of(1945, 7, 24));
        Employee emp9 = new Employee("Comafdescu B.", LocalDate.of(1956, 9, 29));
        Employee emp10 = new Employee("Coffcmcscu B.", LocalDate.of(1969, 1, 24));
        Employee emp11 = new Employee("Comangbncu B.", LocalDate.of(1978, 3, 5));
        Employee emp12 = new Employee("Comadfsfdfsu B.", LocalDate.of(1948, 6, 28));
        Employee emp13 = new Employee("Comjkjjacu B.", LocalDate.of(1986, 5, 7));
        Employee emp14 = new Employee("Comakiuiygfu B.", LocalDate.of(1999, 7, 2));
        Employee emp15 = new Employee("Cohjjkecu B.", LocalDate.of(1979, 11, 4));
        Employee emp16 = new Employee("Aricescu H.", LocalDate.of(1989, 1, 7));
        Employee emp17 = new Employee("Aricfjbdescu H.", LocalDate.of(1978, 12, 9));
        Employee emp18 = new Employee("Aricdhdgvescu H.", LocalDate.of(1941, 7, 17));
        Employee emp19 = new Employee("Aricdedjhejescu H.", LocalDate.of(1986, 6, 6));
        Employee emp20 = new Employee("Aricpsjehvfescu H.", LocalDate.of(1987, 12, 31));
        Employee emp21 = new Employee("Ariedskjsdescu H.", LocalDate.of(1974, 6, 27));
        Employee emp22 = new Employee("Aricesdshgvfeghcu H.", LocalDate.of(1956, 9, 9));
        Employee emp23 = new Employee("Aricjhginvescu H.", LocalDate.of(1949, 8, 16));
        Employee emp24 = new Employee("Aricoepescu H.", LocalDate.of(1999, 3, 14));
        Employee emp25 = new Employee("Cristescu M.", LocalDate.of(1976, 3, 17));
        Employee emp26 = new Employee("Crifdhstescu M.", LocalDate.of(1972, 5, 19));
        Employee emp27 = new Employee("Cristkikscu M.", LocalDate.of(1996, 4, 14));
        Employee emp28 = new Employee("Criwsbstescu M.", LocalDate.of(1973, 11, 25));
        Employee emp29 = new Employee("Cristoooescu M.", LocalDate.of(1968, 2, 12));
        Employee emp30 = new Employee("Cristepovvscu M.", LocalDate.of(1991, 9, 2));
        Employee emp31 = new Employee("Davidescu O.", LocalDate.of(1944, 9, 8));
        Employee emp32 = new Employee("Davidefgscu O.", LocalDate.of(1948, 5, 14));
        Employee emp33 = new Employee("Daviasasdescu O.", LocalDate.of(1947, 4, 18));
        Employee emp34 = new Employee("Daddvidescu O.", LocalDate.of(1951, 3, 28));
        Employee emp35 = new Employee("Davibdbgescu O.", LocalDate.of(1966, 7, 3));
        Employee emp36 = new Employee("Davidebfdscu O.", LocalDate.of(1924, 10, 5));
        Employee emp37 = new Employee("Davsbgmidescu O.", LocalDate.of(1946, 12, 16));
        Employee emp38 = new Employee("Davidddgebscu O.", LocalDate.of(1979, 7, 4));
        Employee emp39 = new Employee("Davnhnhidescu O.", LocalDate.of(1987, 1, 17));
        Employee emp40 = new Employee("Daviuuscu O.", LocalDate.of(1957, 4, 2));

        Company company1 = new Company("Naval", new Employee[]{emp25, emp26, emp27, emp28, emp29, emp30});
        Company company2 = new Company("Auto", new Employee[]{emp1, emp16, emp17, emp18, emp19, emp20, emp21, emp22, emp23, emp24});
        Company company3 = new Company("Plane", new Employee[]{emp2, emp3, emp4, emp5, emp6, emp7});
        Company company4 = new Company("Train", new Employee[]{emp8, emp9, emp10, emp11, emp12, emp13, emp14, emp15});
        Company company5 = new Company("Bike", new Employee[]{emp31, emp32, emp33, emp34, emp35, emp36, emp37, emp38, emp39, emp40, new Employee("Popescu A.", LocalDate.of(1990, 12, 25))});
        Company companiaNoastra = new Company("TheBest30Iunie", new Employee[]{emp25,emp12,angajat1,angajat2,angajat3,angajat4, new Employee("VenitAcum Ghe.", LocalDate.of(1955, 12,25))});
        Company companiaAltora = new Company("TheWorst30Iunie", new Employee[]{});



        company1.getAverageAge();
        company2.getAverageAge();
        company3.getAverageAge();
        company4.getAverageAge();
        company5.getAverageAge();
        companiaNoastra.getAverageAge();
//        companiaAltora.getAverageAge();

    }
}