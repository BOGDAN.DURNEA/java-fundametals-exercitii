package com.sda.company.version2;

public class Company {
    private final String name;
    private Employee[] employees;
    public Company(String name, Employee[] employees) {
        this.name = name;
        this.employees = employees;
    }

    public void setEmployees(Employee[] employees) {
        this.employees = employees;
    }


    public int getAverageAge() {
        int ageTotalEmployees = 0;
        for (Employee i : employees) {
//            if (employees == null){
//                System.out.println("The company " + this.name + "has no employees");
//                return ageTotalEmployees;
//            }

            ageTotalEmployees += i.findAge(); //ageTotalEmployees = ageTotalEmployees + i.findAge()
        }
        int averageAge = ageTotalEmployees / employees.length;
        System.out.println("The average age for the " + employees.length + " employees of the " + this.name + " company is " + averageAge + " years.");
        return averageAge;
    }
}
