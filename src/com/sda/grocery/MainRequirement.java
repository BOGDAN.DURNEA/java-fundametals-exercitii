package com.sda.grocery;

public class MainRequirement {

    /**
     * Grocery Shopping
     * ● Create class Product,it should contain at least two fields –
     * name and price .
     * ● Create an empty array of Products –it’s size should be at least 5.
     * ● Populate the array with some products -this array represents
     * the menu for the user.
     * ● Simulate the process of doing shopping:
     * ○ ask for a product,
     * ○ add it to the cart(array),
     * ○ change index,
     * ○ if index will be greater than 5 – finish shopping,
     * ○ pay for the products.
     */


    public static void main(String[] args) {
        //Product[] productList = new Product[7];

        Product product1 = new Product("chocolate", 10, 1);
        Product product2 = new Product("spaghetti", 15, 2);
        Product product3 = new Product("bread", 20, 3);
        Product product4 = new Product("pork", 5, 2);
        Product product5 = new Product("mici", 25.5, 1);
        Product product6 = new Product("sugar", 19.9, 2);
        Product product7 = new Product("wheat", 11.4, 3);

        // productList[0]= product1;
        // productList[1]= product2;
        // productList[2]= product3;
        // productList[3]= product4;
        // productList[4]= product5;
        // productList[5]= product6;
        // productList[6]= product7;

        Product[] productList = new Product[]{product1, product2, product3, product4, product5, product6, product7};

       Product[] userMenu = productList;



    }
}
