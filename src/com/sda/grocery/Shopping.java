package com.sda.grocery;

import java.util.Scanner;

public class Shopping {
    private Product[] productArray;

    public Product addToCart(Product productToBuy) {
        Product[] shoppingCart = new Product[5];
        for (int i = 0; i < 5; i++) {
            if (shoppingCart[i] == null){
             shoppingCart[i] = productToBuy;
        }
    }

    public Product askForAProduct() {
        System.out.println("What product you are looking for?");
        Scanner scanner = new Scanner(System.in);
        String askedProduct = scanner.nextLine();


        //Product productAsked = new Product();
        for (Product i : productArray) {
            if (i.toString() == askedProduct) {
                return i;
            } else {
                System.out.println("Out of Stock");
            }
        }
        return null;
    }
}