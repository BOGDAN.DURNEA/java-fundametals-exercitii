package com.sda.grocery;
public class Product {
    private String name;
    private double price;
    private int quality;

    public Product() {

    }
    public Product(String name, double price, int quality) {
        this.name = name;
        this.price = price;
        this.quality = quality;
    }

    @Override
    public String toString() {
        return this.name;


    }
}
